<?php
/**
 * Created by PhpStorm.
 * User: nikilai
 * Date: 26.12.18
 * Time: 20:54
 */

namespace AppBundle\Command;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CreateUpdateUserCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('uip-test:todo:user')
            ->setDescription('Create or update users.')
            ->setHelp('Create or update users.')
            ->addArgument('login', InputArgument::REQUIRED, '.')
            ->addArgument('password', InputArgument::REQUIRED, 'The login of the user.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var ContainerInterface $container
         */
        $container = $this->getApplication()->getKernel()->getContainer();
        /**
         * @var EntityManager $em
         */
        $em = $container->get("doctrine.orm.entity_manager");
        $user = $em->getRepository("AppBundle:User")->findOneByUsername($input->getArgument('login'));
        $encoder = $container->get('security.password_encoder');

        if ($user == null) {
            $user = new User();
            $user->setUsername($input->getArgument('login'))->setRoles(["ROLE_USER"]);
            $message = "created";
        } else {
            $message = "updated";
        }
        $password = $encoder->encodePassword($user, $input->getArgument('password'));
        $user->setPassword($password);
        $em->persist($user);
        $em->flush();
        $output->writeln("User successfully " . $message);
    }
}