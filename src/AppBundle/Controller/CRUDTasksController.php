<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Task;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\Annotations as FOSRest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\Delete;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class CRUDTasksController extends FOSRestController
{
    /**
     * @Get("/api/tasks.json", name="get-tasks")
     * @ApiDoc(
     *  description="Get tasks list owned to this user.",
     *  parameters={
     *      {"name"="limit", "dataType"="integer", "required"=false, "description"="limit"},
     *      {"name"="offset", "dataType"="integer", "required"=false, "description"="offset"}
     *  },
     *     statusCodes={
     *         200="Returned objects list",
     *         400="Erroneous parameters",
     *         404="Access denied",
     *     },
     *     output={"collection"=true, "collectionName"="json", "class"="AppBundle\Task"}
     * )
     */
    public function getTasksListAction(Request $request)
    {

        $limit = $request->query->get("limit");
        $offset = $request->query->get("offset");
        if (empty($limit)) {
            $limit = 100;
        } elseif (is_array($limit) || !preg_match("/^[1-9][0-9]*$/", $limit) || $limit < 1 || $limit > 100) {
            return new Response("", 400);
        }
        if (empty($offset)) {
            $offset = 0;
        } elseif (is_array($offset) || !preg_match("/^[0-9][0-9]*$/", $offset) || $offset < 0) {
            return new Response("", 400);
        }
        $em = $this->getDoctrine()->getManager();

        $tasks = $em->getRepository('AppBundle:Task')->findBy(["user" => $this->getUser()], ["lft" => "ASC"], $limit, $offset);


        return $tasks;
    }

    /**
     * @Delete("/api/{id}.json", name="delete-task")
     * @ApiDoc(
     *  description="Deletes the specified task.",
     *  requirements={
     *      {
     *          "name"="id",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="object identifier to delete"
     *      }
     *  },
     *     statusCodes={
     *         204="Returned when successful",
     *         403="Returned when the user is not authorized or task is not owned to this user",
     *         404="Returned when the task is not found",
     *     }
     * )
     */
    public function deleteTaskAction($id)
    {

        if (preg_match("/^[1-9][0-9]*$/", $id)) {
            $em = $this->getDoctrine()->getManager();
            $task = $em->find("AppBundle:Task", $id);

            if ($task != null) {
                if ($task->getUser() != $this->getUser())
                    return new Response('', 403);
                $em->remove($task);
                $em->flush();
            } else {
                return new Response('', 404);
            }
        } else {
            return new Response('', 404);
        }

    }

    /**
     * @Post("/api/tasks.json", name="create-task")
     * @ApiDoc(
     *  description="Create a new task.",
     *  parameters={
     *      {"name"="parent", "dataType"="integer", "required"=false, "description"="task parent identificator"},
     *  },
     *  requirements={
     *      {
     *          "name"="name",
     *          "dataType"="string",
     *          "description"="new task name",
     *          "requirement"="less than 255",
     *      }
     *  },
     *     statusCodes={
     *         201="Success",
     *         400="Erroneous parameters",
     *         404="Access denied",
     *     }
     * )
     */
    public function createTaskAction(Request $request)
    {

        $name = $request->request->get('name');
        $parentId = $request->request->get('parent');
        $em = $this->getDoctrine()->getManager();
        if (!empty($name) && !is_array($name) && strlen($name) <= 255) {
            $newTask = new Task();
            $newTask->setName($name)->setUser($this->getUser());
        } else {
            echo 1;
            return new Response('', 400);
        }
        if (!empty($parentId) && !is_array($parentId) && preg_match("/^[1-9][0-9]*$/", $parentId)) {
            $task = $em->find("AppBundle:Task", $parentId);

            if ($task != null) {
                if ($task->getUser() != $this->getUser()) {

                    return new Response('', 400);
                }

            }

        } else {
            return new Response('', 400);
        }
        $newTask->setParent($task);
        $em->persist($newTask);
        $em->flush();
        return new Response('', 201);

    }

    /**
     * @Put("/api/tasks.json", name="update-task")
     * @ApiDoc(
     *  description="Update the task.",
     *  parameters={
     *      {"name"="parent", "dataType"="integer", "required"=false, "description"="new task parent identificator"},
     *  },
     *  requirements={
     *      {"name"="id", "dataType"="integer",  "requirement"="\d+", "description"="task identificator"},
     *      {
     *          "name"="name",
     *          "dataType"="string",
     *          "description"="new task name",
     *          "requirement"="less than 255",
     *      }
     *  },
     *     statusCodes={
     *         204="Success",
     *         400="Erroneous parameters",
     *         404="Not found",
     *         403="Access denied"
     *     }
     * )
     */
    public function updateTaskAction(Request $request)
    {

        $name = $request->request->get('name');
        $parentId = $request->request->get('parent');
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        if (!empty($id) && !is_array($id) && preg_match("/^[1-9][0-9]*$/", $id)) {
            $forUpdateTask = $em->find("AppBundle:Task", $id);

            if ($forUpdateTask != null) {
                if ($forUpdateTask->getUser() != $this->getUser()) {

                    return new Response('', 400);
                }

            }
            else{
                return new Response('', 404);
            }

        } else {
            return new Response('', 400);
        }
        if (!empty($name) && !is_array($name) && strlen($name) <= 255) {
            $newTask = new Task();
            $newTask->setName($name)->setUser($this->getUser());
        } else {
            return new Response('', 400);
        }

        if (!empty($parentId) && !is_array($parentId) && preg_match("/^[1-9][0-9]*$/", $parentId)) {
            $task = $em->find("AppBundle:Task", $parentId);

            if ($task != null) {
                if ($task->getUser() != $this->getUser()) {

                    return new Response('', 400);
                }

            }

        } else {
            return new Response('', 400);
        }

        $forUpdateTask->setParent($task)->setName($name);
        $em->persist($newTask);
        $em->flush();
        return new Response('', 201);

    }
}
